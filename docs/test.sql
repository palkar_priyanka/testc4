-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 10, 2021 at 09:34 AM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 7.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `test`
--

-- --------------------------------------------------------

--
-- Table structure for table `dept_master`
--

CREATE TABLE `dept_master` (
  `dept_id` int(11) NOT NULL,
  `dept_name` varchar(150) NOT NULL,
  `added_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dept_master`
--

INSERT INTO `dept_master` (`dept_id`, `dept_name`, `added_on`, `updated_on`) VALUES
(1, 'ACCOUNTING', '2021-04-09 14:31:22', '2021-04-09 14:31:22'),
(2, 'RESEARCH', '2021-04-09 14:31:39', '2021-04-09 14:31:39'),
(3, 'SALES', '2021-04-09 14:31:39', '2021-04-09 14:31:39'),
(4, 'OPERATIONS', '2021-04-09 14:32:04', '2021-04-09 14:32:04');

-- --------------------------------------------------------

--
-- Table structure for table `sub_dept`
--

CREATE TABLE `sub_dept` (
  `sub_dept_id` int(11) NOT NULL,
  `dept_id` int(11) NOT NULL,
  `sub_dept_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sub_dept`
--

INSERT INTO `sub_dept` (`sub_dept_id`, `dept_id`, `sub_dept_name`) VALUES
(1, 1, 'CLERK'),
(2, 1, 'MANAGER'),
(3, 2, 'ANALYST'),
(4, 2, 'CLERK'),
(5, 3, 'SALESMAN'),
(6, 3, 'MANAGER'),
(7, 4, 'ASSOCIAT'),
(8, 4, 'ANALYST');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `email_id` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `profile_image` varchar(250) NOT NULL,
  `department` varchar(20) NOT NULL,
  `sub_department` varchar(20) NOT NULL,
  `added_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `first_name`, `last_name`, `email_id`, `password`, `profile_image`, `department`, `sub_department`, `added_on`, `updated_on`) VALUES
(1, 'priyanka', 'palkar', 'priyanka.palkar@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'uploads/download (1).jpg', 'RESEARCH', 'CLERK', '2021-04-10 12:23:21', '2021-04-10 12:24:24'),
(2, 'piya', 'palkar', 'piya.palkar@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'uploads/download (3).jpg', 'OPERATIONS', 'ASSOCIAT', '2021-04-10 12:51:51', '2021-04-10 12:52:14');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dept_master`
--
ALTER TABLE `dept_master`
  ADD PRIMARY KEY (`dept_id`);

--
-- Indexes for table `sub_dept`
--
ALTER TABLE `sub_dept`
  ADD PRIMARY KEY (`sub_dept_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `dept_master`
--
ALTER TABLE `dept_master`
  MODIFY `dept_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `sub_dept`
--
ALTER TABLE `sub_dept`
  MODIFY `sub_dept_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
