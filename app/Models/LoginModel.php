<?php
namespace App\Models;
// use CodeIgniter\Database\ConnectionInterface;
use CodeIgniter\Model;

class LoginModel extends Model {
  /**
     * getAdminData using username and password
     * return records
     */
    public function checkEmailId($email) {
        // print($data);die();
        $query = $this->db->query("SELECT `email_id` FROM `user` WHERE `email_id` = '$email'");
        $result = $query->getResultArray();
        return $result;
    }

   public function validateUser($email,$password) {
        // print($email);die();
        $pass = md5($password);
        $query = $this->db->query("SELECT * FROM `user` WHERE `email_id` = '$email' AND `password` = '$pass'");
        $result = $query->getResultArray();
        return $result;
    }

    public function insertUserData($inputData){

        $first_name = $inputData['first_name'];
        $last_name = $inputData['last_name'];
        $email_id = $inputData['email'];
        $password = md5($inputData['password']);
        
        $query = $this->db->query("INSERT INTO `user` 
        (`user_id`, `first_name`, `last_name`, `email_id`, `password`, `profile_image`, `department` , `sub_department`, `added_on`, `updated_on`) 
        VALUES (NULL, '$first_name', '$last_name', '$email_id', '$password', '','','', NOW(), NOW())");
        $inserted_id = $this->db->insertID();
        return $inserted_id;
        // return $query;;
    }

    public function getUserData($user_id){
        
        $query = $this->db->query("SELECT * FROM `user` WHERE `user_id` = '$user_id'");
        $result = $query->getResultArray();
        return $result;
    }

    public function getDeptData(){
        $query = $this->db->query("SELECT * FROM `dept_master` ");
        $result = $query->getResultArray();
        return $result;
    }

    public function getSubDeptData($dept_id){
        $query = $this->db->query("SELECT * FROM `sub_dept` WHERE `dept_id` = '$dept_id' ");
        $result = $query->getResultArray();
        // print_r($result);
        return $result;
    }

    public function getDeptName($department_id){
        $query = $this->db->query("SELECT `dept_name` FROM `dept_master` WHERE `dept_id` = '$department_id' ");
        $result = $query->getResultArray();
        // print_r($result);
        return $result;
    }

    public function getSubDeptName($sub_department_id){
        $query = $this->db->query("SELECT `sub_dept_name` FROM `sub_dept` WHERE `sub_dept_id` = '$sub_department_id' ");
        $result = $query->getResultArray();
        // print_r($result);
        return $result;
    }

    public function updateData($updatedData){

        $user_id = $updatedData['user_id'];
        $department = $updatedData['department'];
        $sub_department = $updatedData['sub_department'];
        $profile_image = $updatedData['profile_image'];
        
        $query = $this->db->query("UPDATE `user` SET 
            `department` = '$department',
            `sub_department` = '$sub_department', 
            `profile_image` = '$profile_image',  
            `updated_on` =  NOW() WHERE `user_id` = '$user_id' ");

        return $query;
    }

}
?>