<?php

//if this function return true then user is logged in
function user_authorization() {
    if (isset($_SESSION['first_name']) && $_SESSION['first_name'] != null) {
        return true;
    }  
    return false;
}

function authorize() {
    if(user_authorization() == false) {
         return redirect()->to(base_url().'/Login/login');
    }
}



?>