<?php
namespace App\Controllers;
use App\Controllers\BaseController;
use CodeIgniter\Controller;
use CodeIgniter\Model;
use App\Models\LoginModel;
use CodeIgniter\HTTP\RequestInterface;
$this->session = \Config\Services::session();

class Login extends BaseController {
    
    public function login(){

        // if( user_authorization() ) {
        //      return redirect()->to(base_url().'/Login/dashboard');
        // }

        // user_authorization();
        $data = array(
            'page' => "Login",
            'action' => base_url().'/Login/loginAction',
            'name' => 'userlogin'
        );
        // print_r($data);die();
        echo view('header',$data);
        echo view('login_form',$data);
        echo view('footer',$data);
    }

    public function loginAction() {
        //load model
        $model = new LoginModel();
        $this->LoginModel=$model;

        $email = $this->request->getVar('email');
        // print($email);die();

       if(isset($email) && $email != "") {

           $user_data = $this->LoginModel->checkEmailId($email);
           // print_r($user_data);die();

            if(count($user_data) > 0) {
                $data = array(
                    'page' => "pass_page",
                    'email' => $user_data[0]['email_id'],
                    'action' => base_url().'/Login/login_Action',
                    'name' => 'afterpass'
                );
                echo view('header',$data);
                echo view('login_form',$data);
                echo view('footer',$data);

            } else { 
                // $this->session->set_flashdata('error_msg', 'Username or password is incorrect');
                $data = array(
                    'page' => "sign_page",
                    'email' => $email,
                    'action' => base_url().'/Login/signup',
                    'name' => 'signup'
                );
                echo view('header',$data);
                echo view('sign_form',$data);
                echo view('footer',$data);
            }
        }
    }

    public function login_Action(){
        
        // user_authorization();
        $model = new LoginModel();
        $this->LoginModel=$model;

        $email = $this->request->getVar('email');
        $password = $this->request->getVar('password');

        if(isset($email) && $email != "" && isset($password) && $password != "") {
            $user_data = $this->LoginModel->validateUser($email,$password);
            // print_r($user_data);
            if(count($user_data) > 0) {
                foreach ($user_data as $row)
                {
                    $user_id = $row['user_id'];
                    $first_name = $row['first_name'];
                    $last_name = $row['last_name'];
                    $email_id = $row['email_id'];
                    $profile_image = $row['profile_image'];
                }
                // $session = \Config\Services::session();
                // $session_id = session_id();
                $this->session->set('user_id',$user_id);
                $this->session->set('first_name',$first_name);
                $this->session->set('last_name',$last_name);
                $this->session->set('email_id',$email_id);
                $this->session->set('profile_image',$profile_image);
                $this->session->set('from_login','from_login');
                // echo "here";die();
                $this->session->setFlashdata('success','Logged In Successfuly...!');
                return redirect()->to(base_url().'/Login/dashboard');
            }else{
                $this->session->setFlashdata('error_msg','Username or password is incorrect');
                return redirect()->to(base_url().'/Login/login');
            }
        }
    }

    public function signup(){
        // user_authorization();

        $model = new LoginModel();
        $this->LoginModel=$model;

        $inputData['first_name'] = $this->request->getVar('first_name');
        $inputData['last_name'] = $this->request->getVar('last_name');
        $inputData['email'] = $this->request->getVar('email');
        $inputData['password'] = $this->request->getVar('password');

        if(isset($inputData['first_name']) && $inputData['first_name'] != '' && isset($inputData['last_name']) && $inputData['last_name'] != '' && isset($inputData['email']) && $inputData['email'] != '' && isset($inputData['password']) && $inputData['password'] != ''){
            $user_id = $this->LoginModel->insertUserData($inputData);
            if($user_id){
                // $session = \Config\Services::session();
                // $session_id = session_id();
                $this->session->set('user_id',$user_id);
                $this->session->set('first_name',$inputData['first_name']);
                $this->session->set('from_sign_up','from_sign_up');
                // $this->session->setFlashdata('success','Logged In Successfuly...!');
                return redirect()->to(base_url().'/Login/dashboard');
            }
        }
    }

    public function dashboard() {
        user_authorization();
        $model = new LoginModel();
        $this->LoginModel=$model;

        if($this->session->get('from_login') == 'from_login'){
            $data = array(
                'page' => "dashboard",
                'action' => '',
                'name' => 'profile'
            ); 

            echo view('header',$data);
            echo view('landing_page',$data);
            echo view('footer',$data);

        }

        if($this->session->get('from_sign_up') == 'from_sign_up'){

            $user_id = $this->session->get('user_id');
            $get_user_info = $this->LoginModel->getUserData($user_id);

            $user_info = array();
            foreach ($get_user_info as $value) {
                $user_info = array(
                    'user_id' => $get_user_info[0]['user_id'],
                    'first_name' => $get_user_info[0]['first_name'],
                    'last_name' => $get_user_info[0]['last_name'],
                    'email' => $get_user_info[0]['email_id']
                );   
            }

            $get_dept_info = $this->LoginModel->getDeptData();

            $data = array(
                'page' => "dashboard",
                'email' => $email,
                'action' => base_url().'/Login/profile_info',
                'name' => 'profile',
                'user_info' =>  $user_info,
                'dept_info' => $get_dept_info
            );
            echo view('header',$data);
            echo view('profile_form',$data);
            echo view('footer',$data);
        }
    }

    public function get_sub_dept(){
        // user_authorization();
        $model = new LoginModel();
        $this->LoginModel=$model;

        $dept_id = $this->request->getVar('dept_id');

        $get_sub_dept_data = $this->LoginModel->getSubDeptData($dept_id);
        // print_r($get_sub_dept_data);
        echo json_encode($get_sub_dept_data,true);

    }

    public function profile_info(){
        // user_authorization();
        $model = new LoginModel();
        $this->LoginModel=$model;

        $updatedData['user_id'] = $this->request->getVar('user_id');
        $department_id = $this->request->getVar('department');
        $getDeptName = $this->LoginModel->getDeptName($department_id);
        $updatedData['department'] = $getDeptName[0]['dept_name'];
        $sub_department_id = $this->request->getVar('sub_department');
        $getSubDeptName = $this->LoginModel->getSubDeptName($sub_department_id);
        $updatedData['sub_department'] = $getSubDeptName[0]['sub_dept_name'];

        if($updatedData['user_id']){
            $target_dir = "uploads/";

            $file_name = basename($_FILES["profile_photo"]["name"]);

            if($file_name == ''){
                $target_file = '';
            }else{
                $target_file = $target_dir . basename($_FILES["profile_photo"]["name"]);
            }

            if (move_uploaded_file($_FILES["profile_photo"]["tmp_name"], $target_file)) {
                $updatedData['profile_image'] = $target_file;
                $result = $this->LoginModel->updateData($updatedData);
                $this->session->setFlashdata('success','Profile Updated Successfuly...!');
            }
        }

            $user_id = $this->session->get('user_id');
            $get_user_info = $this->LoginModel->getUserData($user_id);
            // print_r($get_user_info);die();

            $user_info = array();
            foreach ($get_user_info as $value) {
                $user_info = array(
                    'user_id' => $get_user_info[0]['user_id'],
                    'first_name' => $get_user_info[0]['first_name'],
                    'last_name' => $get_user_info[0]['last_name'],
                    'email' => $get_user_info[0]['email_id'],
                    'profile_image' => $get_user_info[0]['profile_image'],
                    'department' => $get_user_info[0]['department'],
                    'sub_department' => $get_user_info[0]['sub_department'],
                );   
            }

            $data = array(
                'page' => "Updated_profile_info",
                'profile_data' => $user_info,
            );
            echo view('header',$data);
            echo view('profile_information',$data);
            echo view('footer',$data);
    }

    public function logout(){
        // user_authorization();
        $this->session->destroy();
        $this->session->setFlashdata('success','Logged Out successfully');
        return redirect()->to(base_url().'/Login/login');
    }

}
?>