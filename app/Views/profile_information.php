
<div class="container-fluid">
	<div class="row justify-content-center">
		<div class="col-12 col-xl-5 col-lg-5 ">
			<div class="jumbotron jumbotron-fluid" style="margin-top:10%;">
				<div class="container text-center">
					<?php if($page == 'Updated_profile_info'){ ?>
						<h4>Profile Information</h4>
						<div class="form-label-group">
							<div class="form-row" >
								<div class="col-md-6 text-center"><h5><b>First Name</b></h5></div>
								<div class="col-md-6 text-center"><input type="text" class="form-control" value="<?php echo $profile_data['first_name'] ?>"></div>
							</div>
							<div class="form-row" style="margin-top: 8px;">
								<div class="col-md-6 text-center"><h5><b>Last Name</b></h5></div>
								<div class="col-md-6 text-center"><input type="text" class="form-control" value="<?php echo $profile_data['last_name'] ?>"></div>
							</div>
							<div class="form-row" style="margin-top: 8px;">
								<div class="col-md-6 text-center"><h5><b>Email Address</b></h5></div>
								<div class="col-md-6 text-center"><input type="text" class="form-control" value="<?php echo $profile_data['email'] ?>"></div>
							</div>
							<div class="form-row" style="margin-top: 8px;">
								<div class="col-md-6 text-center"><h5><b>Department</b></h5></div>
								<div class="col-md-6 text-center"><input type="text" class="form-control" value="<?php echo $profile_data['department'] ?>"></div>
							</div>
							<div class="form-row" style="margin-top: 8px;">
								<div class="col-md-6 text-center"><h5><b>Sub Department</b></h5></div>
								<div class="col-md-6 text-center"><input type="text" class="form-control" value="<?php echo $profile_data['sub_department'] ?>"></div>
							</div>
							<div class="form-row" style="margin-top: 8px;">
								<div class="col-md-6 text-center"><h5><b>Profile Picture</b></h5></div>
								<div class="col-md-6 text-center">									<?php if(!empty($profile_data['profile_image'])){ ?>
									<image id="profileImage" src="<?php echo base_url().'/'.$profile_data['profile_image'];?>" title="profile Image" style="border: 1px solid grey;height: 125px;width: 125px;"/>
								<?php }else{ ?>
									<image id="profileImage" src="<?php echo base_url().'/assets/images/default_profile.jpg' ?>" title="profile Image" style="border: 1px solid grey;height: 125px;width: 125px;"/>
								<?php } ?></div>
							</div>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>