
<div class="container-fluid">
	<div class="row justify-content-center">
		<div class="col-12 col-xl-5 col-lg-5 ">
		<form method="post" action="<?php echo $action ?>" name="<?php echo $name ?>" id="<?php echo $name ?>">
			<div class="jumbotron jumbotron-fluid" style="margin-top:10%;">
				<div class="container text-center">
					<?php if($page == 'sign_page'){ ?>
						<h4>Sign Up</h4>
						<br>
						<div class="form-label-group">
							<a id="first_name_error" class=" text-danger"><font style="color:red;weight:bold;"></font></a>

							<input type="text" name="first_name" id="first_name" class="form-control" placeholder="Please Enter First Name" autofocus="" autocomplete="off">

							<a id="last_name_error" class=" text-danger"><font style="color:red;weight:bold;"></font></a>

							<input type="text" name="last_name" id="last_name" class="form-control" placeholder="Please Enter Last Name" autofocus="" autocomplete="off" style="margin-top: 8px;">

							<a id="email_id_error" class=" text-danger"><font style="color:red;weight:bold;"></font></a>

							<input type="email" name="email" id="email" class="form-control" placeholder="Please Enter Email Address" value="<?php echo $email; ?>" autofocus="" autocomplete="off" style="margin-top: 8px;">

							<a id="password_error" class=" text-danger"><font style="color:red;weight:bold;"></font></a>

							<input type="password" name="password" id="password" class="form-control" placeholder="Please Enter Password" autofocus="" autocomplete="off" style="margin-top: 8px;">

							<a href="#" class="btn btn-primary btn-user btn-block" onclick="validateForm();" style="margin-top: 8px;">Login</a>
						</div>
					<?php } ?>
				</div>
			</div>
		</form>
		</div>
	</div>
</div>
<script>
	function validateForm(){
		var error = false;
		var chk_email = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i;
		var first_name = $('#first_name').val();
		var last_name = $('#last_name').val();
		var email_id = $('#email').val();
		var password = $('#password').val();

		if(first_name == ''){
			$('#first_name_error').html("<span class='text-danger'>* Please Enter First Name </span>");
			$('#first_name_error').show();
		}else{
			$('#first_name_error').html("");
		}

		if(last_name == ''){
			$('#last_name_error').html("<span class='text-danger'>* Please Enter Last Name </span>");
			$('#last_name_error').show();
		}else{
			$('#last_name_error').html("");
		}

		if(email_id == ''){
			$('#email_id_error').html("<span class='text-danger'>* Email ID Required</span>");
			$('#email_id_error').show();
			error = true;
		}else if(email_id!='' && chk_email.test(email_id)==false){
			$("#email_id_error").html('Valid Email ID Required.');
			$("#email_id_error").show();
			error=true;
		}else{
			$('#email_id_error').html("");
		}

		if(password == ''){
			$('#password_error').html("<span class='text-danger'>* Please Enter Password </span>");
			$('#password_error').show();
		}else{
			$('#password_error').html("");
		}

		console.log(error);
		if(error == true){
			return false;
		}else{
			$("#signup").submit();
		}
	}
</script>