
<div class="container-fluid">
	<div class="row justify-content-center">
		<div class="col-12 col-xl-5 col-lg-5 ">
		<form method="post" action="<?php echo $action ?>" name="<?php echo $name ?>" id="<?php echo $name ?>" enctype="multipart/form-data">
			<div class="jumbotron jumbotron-fluid" style="margin-top:10%;">
				<div class="container text-center">
					<?php if($page == 'dashboard'){ ?>
						<h4>Profile Information</h4>
						<div class="form-label-group">
							<input type="hidden" name="user_id" id="user_id" value="<?php echo $user_info['user_id']; ?>">
							<a id="first_name_error" class=" text-danger"><font style="color:red;weight:bold;"></font></a>

							<input type="text" name="first_name" id="first_name" class="form-control" placeholder="Please Enter First Name" value="<?php echo $user_info['first_name'] ?>" autofocus="" autocomplete="off">

							<a id="last_name_error" class=" text-danger"><font style="color:red;weight:bold;"></font></a>

							<input type="text" name="last_name" id="last_name" value="<?php echo $user_info['last_name'] ?>" class="form-control" placeholder="Please Enter Last Name" autofocus="" autocomplete="off" style="margin-top: 8px;">

							<a id="email_id_error" class=" text-danger"><font style="color:red;weight:bold;"></font></a>

							<input type="email" name="email" id="email" class="form-control" placeholder="Please Enter Email Address" value="<?php echo $user_info['email']; ?>" autofocus="" autocomplete="off" style="margin-top: 8px;">
							<div class="form-row" style="margin-top: 8px;">
								<label><h5> Upload Profile Picture</h5></label>
							</div>

							<a id="user_profile_error" class=" text-danger"><font style="color:red;weight:bold;"></font></a>

							<div class="form-row" style="margin-top: 8px;">
								<div id="profile-container" class="form-group col-md-2">
									<?php if(!empty($profile_image)){ ?>
										<image id="profileImage" src="<?php echo base_url().'/'.$profile_image;?>" title="profile Image" style="border: 1px solid grey;height: 125px;width: 125px;"/>
									<?php }else{ ?>
										<image id="profileImage" src="<?php echo base_url().'/assets/images/default_profile.jpg' ?>" title="profile Image" style="border: 1px solid grey;height: 125px;width: 125px;"/>
									<?php } ?>
								</div>
								<div class="form-group col-md-3">
									<input id="imageUpload" type="file" accept="image/png,image/jpg,image/jpeg" name="profile_photo" style="margin-top: 97px;margin-left: 40px;">
								</div>
							</div>

							<a id="dept_error" class=" text-danger"><font style="color:red;weight:bold;"></font></a>

							<div class="form-row" style="margin-top: 8px;">
								<!-- <label for="department">Country</label> -->
								<select id="department" name="department" class="form-control">
									<option value="" >Select Department</option>
									<?php foreach($dept_info as $key=>$val){ ?>	
										<option value="<?php echo $val['dept_id']; ?>"  ><?php echo $val['dept_name']; ?></option>
									<?php  }  ?>
								</select>
							</div>

							<a id="sub_dept_error" class=" text-danger"><font style="color:red;weight:bold;"></font></a>

							<div class="form-row" style="margin-top: 8px;">
								<select id="sub_department" name="sub_department" class="form-control">
									<option value="" >Select Sub Department</option>
								</select>
							</div>
							

							<a href="#" class="btn btn-primary btn-user btn-block" onclick="validateForm();" style="margin-top: 8px;">Save</a>
						</div>
					<?php } ?>
				</div>
			</div>
		</form>
		</div>
	</div>
</div>
<script>

function readURL(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function(e) {
			var choosefile = $("#imageUpload").val();
			if(choosefile != ''){
				var fsize = input.files[0].size;
				if(fsize>2000000){
					$("user_profile_error").show();
					$("user_profile_error").html("<span class='text-danger'>* File size is more than 2 MB.</span>");
					// $("#imageUpload").val('');
				}else{
					$('#profileImage').attr('src', e.target.result);
					$("user_profile_error").html('');
					$("user_profile_error").hide();
				}
			}else{
				$('#profileImage').attr('src', e.target.result);
				$("user_profile_error").hide();
			} 	
		}
		reader.readAsDataURL(input.files[0]); // convert to base64 string
	}
}

$("#imageUpload").change(function() {
  readURL(this);
});

$('#department').change( function getMatterData(dept_id){
    
    var dept_id = $(this).val();
    
	var func='get_sub_dept';
	var path='<?php echo base_url()?>/Login/';

	$.ajax({
		url:path+func,	
		type:"POST",
		data:{
			dept_id:dept_id
		},
		success:function(data){

			var sub_dept = JSON.parse(data);
			console.log(sub_dept);
			$('#sub_department').empty();

			for (i = 0; i < sub_dept.length; i++) {
				$('#sub_department').append('<option value="'+sub_dept[i]['sub_dept_id']+'">'+sub_dept[i]['sub_dept_name']+'</option>');
			}
		}
	});   
});

function validateForm(){
	var error = false;
	var chk_email = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i;
	var first_name = $('#first_name').val();
	var last_name = $('#last_name').val();
	var email_id = $('#email').val();
	var profile = $('#imageUpload').val();
	var dept = $('#department').val();
	var sub_dept = $('#sub_department').val();

	if(first_name == ''){
		$('#first_name_error').html("<span class='text-danger'>* Please Enter First Name </span>");
		$('#first_name_error').show();
	}else{
		$('#first_name_error').html("");
	}

	if(last_name == ''){
		$('#last_name_error').html("<span class='text-danger'>* Please Enter Last Name </span>");
		$('#last_name_error').show();
	}else{
		$('#last_name_error').html("");
	}

	if(email_id == ''){
		$('#email_id_error').html("<span class='text-danger'>* Email ID Required</span>");
		$('#email_id_error').show();
		error = true;
	}else if(email_id!='' && chk_email.test(email_id)==false){
		$("#email_id_error").html('Valid Email ID Required.');
		$("#email_id_error").show();
		error=true;
	}else{
		$('#email_id_error').html("");
	}

	if(profile == ''){
		$("#user_profile_error").html("<span class='text-danger'>* Please choose file to upload.</span>");
		$("#user_profile_error").show();
		error=true;
	}else{
		$("#user_profile_error").html("");
		$("#user_profile_error").show();
	}

	if(dept == ''){
		$('#dept_error').html("<span class='text-danger'>* Please Select Department </span>");
		$('#dept_error').show();
		error=true;
	}else{
		$('#dept_error').html("");
	}

	if(sub_dept == ''){
		$('#sub_dept_error').html("<span class='text-danger'>* Please Select Sub Department </span>");
		$('#sub_dept_error').show();
		error=true;
	}else{
		$('#sub_dept_error').html("");
	}

	console.log(error);
	if(error == true){
		return false;
	}else{
		$("#profile").submit();
	}

}

</script>