

<div class="container-fluid">
	<div class="row justify-content-center">
		<div class="col-12 col-xl-5 col-lg-5 ">
		<form method="post" action="<?php echo $action ?>" name="<?php echo $name ?>" id="<?php echo $name ?>">
			<div class="jumbotron jumbotron-fluid" style="margin-top:10%;">
				<div class="container text-center">
					<?php if($page == 'Login'){ ?>
						<!-- <h1 class="display-4">Login Here</h1> -->
						<div class="form-label-group">
							<a id="email_id_error" class=" text-danger"><font style="color:red;weight:bold;"></font></a>
							<input type="email" name="email" id="email" class="form-control" placeholder="Please Enter Email Address" autofocus="" autocomplete="off">
							
							<a href="#" class="btn btn-primary btn-user btn-block" onclick="validateForm();">Login</a>
						</div>
					<?php } ?>
					<?php if($page == 'pass_page'){ ?>
						<!-- <h1 class="display-4">Enter Password</h1> -->
						<div class="form-label-group">
							<a id="password_error" class=" text-danger"><font style="color:red;weight:bold;"></font></a>
							<input type="hidden" name="email" id="email" value="<?php echo (isset($email) ? $email : ''); ?>">
							<input type="password" name="password" id="password" class="form-control" placeholder="Please Enter Password" autofocus="" autocomplete="off">
							<a href="#" class="btn btn-primary btn-user btn-block" onclick="validatePassword();">Login</a>
						</div>
					<?php } ?>
				</div>
			</div>
		</form>
		</div>
	</div>
</div>
<script>
	function validateForm(){
		var error = false;
		var chk_email = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i;
		var email_id = $('#email').val();


		if(email_id == ''){
			$('#email_id_error').html("<span class='text-danger'>* Email ID Required</span>");
			$('#email_id_error').show();
			error = true;
		}else if(email_id!='' && chk_email.test(email_id)==false){
			$("#email_id_error").html('Valid Email ID Required.');
			$("#email_id_error").show();
			error=true;
		}else{
			$('#email_id_error').html("");
		}

		console.log(error);
		if(error == true){
			return false;
		}else{
			$("#userlogin").submit();
		}
	}

	function validatePassword(){
		var error = false;
		var password = $('#password').val();
		
		if(password == ''){
			$('#password_error').html("<span class='text-danger'>* Please Enter Password </span>");
			$('#password_error').show();
		}else{
			$('#password_error').html("");
		}

		console.log(error);
		if(error == true){
			return false;
		}else{
			$("#afterpass").submit();
		}
	}
</script>

