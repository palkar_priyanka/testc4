<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title> TEST </title>
  <link rel="stylesheet" href="<?php echo base_url().'/assets/lib/css/bootstrap.superhero.min.css'; ?>">
  <link rel="stylesheet" href="<?php echo base_url().'/assets/css/common_style.css'; ?>">
  <link rel="stylesheet" href="<?php echo base_url().'/assets/lib/css/font-awesome.min.css'; ?>"> 
  
  <script src="<?php echo base_url().'/assets/lib/js/jquery.min.js'; ?>"></script>
  <script src="<?php echo base_url().'/assets/lib/js/popper.min.js'; ?>"> </script>
  <script src="<?php echo base_url().'/assets/lib/js/bootstrap.min.js'; ?>"> </script>
  <script src="<?php echo base_url().'/assets/lib/js/jquery.datetimepicker.full.min.js'; ?>"> </script>
</head>

<?php
  $this->session = \Config\Services::session();
  $page = isset($page) ? $page : "";
  // $menu = isset($menu) ? $menu : "";
?>

<body>

<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
  <div class="navbar-brand">
    <a href="<?php echo base_url(); ?>">
      <b>TestC4</b>
    </a>
  </div>

<?php if( $this->session->get("first_name") ) { ?>
  <ul class="navbar-nav mr-auto">
  </ul>
  <ul class="navbar-nav">
    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
        Hello <?php echo $this->session->get("first_name"); ?>
      </a>
      <div class="dropdown-menu" style="left:-50px;">
        <a class="dropdown-item" href="<?php echo base_url().'/Login/logout' ; ?>">Logout</a>
      </div>
    </li>
  </ul>
  <?php } else  { ?> 
    <?php if(isset($page) && $page !== 'login_form') { ?> 
      <ul class="navbar-nav ml-auto">
        <li class="nav-item" style="margin-right: 30px">
          <a href="<?php echo base_url().'/Login/login' ; ?>">Login</a>
        </li>
      </ul>
    <?php } ?>
<?php } ?>

</nav>
<br>
<br>
<div class="notification">
  <?php if($this->session->getFlashdata('success') != '') { ?>
    <p class="bg-success text-white text-center">
      <?php echo $this->session->getFlashdata('success'); $this->session->setFlashdata('success','');?>
    </p>
  <?php } ?>

  <?php if($this->session->getFlashdata('error_msg') != '') { ?>
    <p class="bg-danger text-white text-center">
      <?php echo $this->session->getFlashdata('error_msg'); $this->session->setFlashdata('error_msg','');?>
    </p>
  <?php } ?>
</div> 