<?php
namespace App\Controllers;
// session_start();
//if ( ! defined('BASEPATH')) exit('No direct script access allowed');
namespace App\Controllers;
use CodeIgniter\Controller;
use App\Models\Master_model;
use CodeIgniter\HTTP\RequestInterface;
namespace App\Libraries;
class NF_Query_search {
	function save_query($query_array) {
		
		//if(!isset($CI))
		//	$CI =& get_instance();
		$db      = \Config\Database::connect();
		$session = \Config\Services::session();
		$model = new \App\Models\Master_model();
		// $CI->session=$session; 
		// $CI->master_model=$model;
		
		$user_id= $session->get('user_id');
		$added_by_type = 'admin';
		$added_by_id =$user_id;//$sess['admin_id'];
		$module_id = $session->get('session_current_module_id');
		//$CI->db->insert('query_search', array('query_string' => http_build_query($query_array),'module_id' => $module_id,'added_by_type' => $added_by_type, 'added_by_id' => $added_by_id));
		$model->insert_record('query_search', array('query_string' => http_build_query($query_array),'module_id' => $module_id,'added_by_type' => $added_by_type, 'added_by_id' => $added_by_id));
		$insert_id=$db->insertID();
		unset($model);		
		return $insert_id;
	}
	
	function load_query($query_id) {
	//dumpEX($query_id);
		//if(!isset($CI))
		//	$CI =& get_instance();
		$db   = \Config\Database::connect();
		$session = \Config\Services::session();
		$model = new \App\Models\Master_model();
		// $CI->session=$session;
		// $CI->master_model=$model;
		
		//$rows = $CI->master_model->where('query_search', array('query_search_id' => $query_id))->result();
		$condition=array();	
		$condition[] = array('where','query_search_id',$query_id);		
		$select_data=array(
						'table_name'=>'query_search',
						'table_fields'=>'query_string',
						'condition'=>$condition,
						'limit'=>'',
						'offset'=>''
						);
		$query_data=$model->select_data($select_data);
		///echo "---".$CI->master_model->get_last_query();
		foreach ($query_data as $row)
		{
			$query_string = $row['query_string'];
		}
		
		$new_array = array();
		if (isset($query_string)) {
			parse_str($query_string, $new_array);		
		}
		if(is_array($new_array) && !empty($new_array)){
		
			foreach($new_array as $k=>$v){
				$_GET[str_replace('amp;','',$k)] = $v;
			}
			return $_GET;
		}
		//unset($CI);		
	}	
}
